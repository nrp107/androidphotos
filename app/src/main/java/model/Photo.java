package model;

import android.graphics.Bitmap;
import android.net.Uri;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Photo implements Serializable{

    //private String caption;
    public Uri imageUri;
   // private File image;
    //public Date date;
    public String filePath;
    public InputStream imageStream;
    //public Bitmap selectedImage;
    public ProxyBitmap selectedImage;
    //public Calendar calendar;
    public List<Tags> tags = new ArrayList<Tags>();

    public Photo (Uri imageUri)
    {
        this.imageUri=imageUri;

    }
    public Photo (String filePath){
        this.filePath=filePath;
    }
    public Photo(InputStream imageStream)
    {
        this.imageStream=imageStream;
    }
    public Photo(Bitmap selectedImage)
    {
        this.selectedImage=new ProxyBitmap(selectedImage);
    }
//    public Photo (File image) {
//        this.image=image;
//        this.caption="default caption";
//        this.filePath=image.toURI().toString();
//        calendar=new GregorianCalendar();
//        calendar.set(Calendar.MILLISECOND, 0);
//        this.date=calendar.getTime();
//    }
//
//    public Photo(String caption,File image) {
//        this.image=image;
//        this.caption=caption;
//        this.filePath=image.toURI().toString();
//        calendar=new GregorianCalendar();
//        calendar.set(Calendar.MILLISECOND, 0);
//        this.date=calendar.getTime();
//    }

    //public File getImage () {        return image;    }

   // public String filePathProperty () {        return filePath;    }

//    public String photoCaptionProperty() {
//        return caption;
//    }
//
//    public void setCaption(String caption) {
//        this.caption=caption;
//    }
//
//    public String getCaption() {
//        return caption;
//    }
//
//    public String captionProperty() {
//        return caption;
//    }

    public List<Tags> getTags()
    {
        return tags;
    }

    //public String toString() {        return this.filePath;    }
}
