package model;

import android.net.Uri;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Album implements Serializable{

    private static final long serialVersionUID = 1L;

    private String albumName;
    public int photoCount;
    public List<Photo> photos = new ArrayList<Photo>();
    public java.util.Date rangeStart;
    public java.util.Date rangeEnd;

    public Album(String albumName) {
        this.albumName=albumName;
        photoCount=0;
    }

    public String albumNameProperty() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getAlbumName() {
        return albumName;
    }
    public int getPhotoCount() {
        return photoCount;
    }

    public List<Photo> getPhotos(){
        return photos;
    }

    public void addPhoto(String string, Uri pic) {
        //Photo image = new Photo(string, pic);
        Photo image = new Photo(pic);
        photos.add(image);
    }

//    public String getRange() {
//        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
//        if (photos.size()==0)
//        {
//            return " - ";
//        }
//        else if (photos.size()==1)
//        {
//            rangeStart=photos.get(0).date;
//            rangeEnd=photos.get(0).date;
//            return ""+df.format(rangeStart)+" - "+df.format(rangeEnd);
//        }
//        else if (photos.size()>1)
//        {
//            rangeStart=photos.get(0).date;
//            rangeEnd=photos.get(0).date;
//            for (int i=0;i<photos.size();i++)
//            {
//                if (photos.get(i).date.compareTo(rangeStart)<0)
//                    rangeStart=photos.get(i).date;
//                if (photos.get(i).date.compareTo(rangeEnd)>0)
//                    rangeEnd=photos.get(i).date;
//            }
//            return ""+df.format(rangeStart)+" - "+df.format(rangeEnd);
//        }
//        return " - ";
//
//    }

    public String toString() {
        return this.getAlbumName();
    }

}
