package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String storeDir = "dat";
    public static final String storeFile = "users.dat";


    private String userName;
    public List<Album> albums = new ArrayList<Album>();

    public User() {
    }

    public User(String userName) {
        this.userName=userName;
    }

    public User(String userName, Album album) {
        this.userName=userName;
        this.albums.add(album);

    }

    public List<Album> getAlbum(){
        return albums;
    }

    public String userNameProperty() {
        return userName;
    }

    public void setUser(String userName) {
        this.userName=userName;
    }

    public String getUserName() {
        return userName;
    }

    public void createAlbum(String albumName) {
        Album album = new Album(albumName);
        albums.add(album);
    }

    public String toString() {
        return this.getUserName();
    }

}
