package model;

import java.io.Serializable;

public class Tags implements Serializable{

    public String tag;
    public int tagNum= 0;

    public Tags(String tag) {
        this.tag=tag;
    }

    public String tagsProperty () {
        return tag;
    }

    public String getTags() {
        return tag;
    }

    public String toString() {
        return tag;
    }
}
