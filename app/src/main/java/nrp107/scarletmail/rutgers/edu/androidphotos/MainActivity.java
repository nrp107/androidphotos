package nrp107.scarletmail.rutgers.edu.androidphotos;
//Nidhi Patel and Vincent Ren

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import model.Album;

public class MainActivity extends AppCompatActivity {

    ListView albumListView;
    EditText albumEditText;
    Button addAlbumButton;
    Button renameAlbumButton;
    Button deleteAlbumButton;
    Button openAlbumButton;
    Button searchButton;
    //public final Context fixedContext=this;

    static ArrayList<Album>albumArrayList=new ArrayList<Album>();
    static ArrayAdapter<Album> adapter;

    static int albumIndex;
    public File filename= new File("/data/user/0/nrp107.scarletmail.rutgers.edu.androidphotos/files/data6.dat");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try{
            albumArrayList=load();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        albumListView=(ListView) findViewById(R.id.albumListView);
        albumEditText=(EditText) findViewById(R.id.albumEditText);
        addAlbumButton=(Button) findViewById(R.id.addAlbumButton);
        renameAlbumButton=(Button) findViewById(R.id.renameAlbumButton);
        deleteAlbumButton=(Button) findViewById(R.id.deleteaAlbumButton);
        openAlbumButton=(Button)findViewById(R.id.openAlbumButton);
        searchButton =(Button)findViewById(R.id.searchButton);

        if (!filename.exists())
        {
            Context context=this;
            File file = new File(context.getFilesDir(),"data6.dat");
            try {
                file.createNewFile();
            } catch (IOException e) {

            }
        }
        //readSerializable(getApplicationContext());

        adapter=new ArrayAdapter<Album>(this, android.R.layout.simple_list_item_single_choice,albumArrayList );
        albumListView.setAdapter(adapter);

        albumListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                albumEditText.setText(albumArrayList.get(position).getAlbumName());
            }
        });

        addAlbumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    add();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        renameAlbumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rename();
                try {
                    save(albumArrayList);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        deleteAlbumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
                try {
                    save(albumArrayList);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        openAlbumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (albumListView.getCheckedItemCount()==1)
                {
                    albumIndex = albumListView.getCheckedItemPosition();
                    //Toast.makeText(getApplicationContext(),"album index is "+albumIndex, Toast.LENGTH_SHORT).show();
                    Intent albumIntent = new Intent(getApplicationContext(), AlbumActivity.class);
                    //Toast.makeText(getApplicationContext(),"length of photos array is "+albumArrayList.get(albumIndex).getPhotos().size(), Toast.LENGTH_SHORT).show();

                    startActivity(albumIntent);
                }
                else
                    Toast.makeText(getApplicationContext(),"Must select an album to open", Toast.LENGTH_SHORT).show();
            }
        });
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent searchIntent = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(searchIntent);
            }
        });
    }

    private void add() throws IOException {
        if (albumEditText.getText().toString().length()>0)
        {
            Album album=new Album(albumEditText.getText().toString().toLowerCase());
            //check for duplicate
            boolean duplicate=false;
            for (int i=0;i<albumArrayList.size();i++)
            {
                if (albumEditText.getText().toString().toLowerCase().equals(albumArrayList.get(i).getAlbumName())){
                    duplicate = true;
                    Toast.makeText(getApplicationContext(),"Album already exists", Toast.LENGTH_SHORT).show();
                    break;
                }
            }
            if (duplicate==false) {
                adapter.add(album);
                adapter.notifyDataSetChanged();
                albumEditText.setText("");
                Toast.makeText(getApplicationContext(), "Successfully added album", Toast.LENGTH_SHORT).show();
                //saveSerializable(getApplicationContext(),albumArrayList);
                try {
                    save(albumArrayList);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else
            Toast.makeText(getApplicationContext(),"Must enter name to add new album", Toast.LENGTH_SHORT).show();

    }

    public void rename()
    {
        String album=albumEditText.getText().toString().toLowerCase();
        int position = albumListView.getCheckedItemPosition();

        if (album.length()>0) {
            boolean duplicate = false;
            for (int i = 0; i < albumArrayList.size(); i++) {
                if (album.equals(albumArrayList.get(i).getAlbumName())) {
                    duplicate = true;
                    Toast.makeText(getApplicationContext(), "Album already exists", Toast.LENGTH_SHORT).show();
                    break;
                }
            }
            if (duplicate == false) {
                albumArrayList.get(position).setAlbumName(album);
                adapter.notifyDataSetChanged();
                albumEditText.setText("");
                Toast.makeText(getApplicationContext(), "Successfully renamed album", Toast.LENGTH_SHORT).show();
               // saveSerializable(getApplicationContext(),albumArrayList);
            }
        }
        else
            Toast.makeText(getApplicationContext(),"Must select album to rename", Toast.LENGTH_SHORT).show();

    }

    public void delete()
    {
        int position = albumListView.getCheckedItemPosition();
        if (position>-1)
        {
            adapter.remove(albumArrayList.get(position));
            adapter.notifyDataSetChanged();
            albumEditText.setText("");
            Toast.makeText(getApplicationContext(),"Successfully deleted album ", Toast.LENGTH_SHORT).show();
            //saveSerializable(getApplicationContext(),albumArrayList);
        }
    else
            Toast.makeText(getApplicationContext(),"Must select album to delete", Toast.LENGTH_SHORT).show();

    }
    public static void save(ArrayList<Album> albumList) throws IOException{
        ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream("/data/user/0/nrp107.scarletmail.rutgers.edu.androidphotos/files/data6.dat"));
        oos.writeObject(albumList);
        oos.close();

    }
    public static ArrayList<Album> load() throws IOException, ClassNotFoundException{
        ObjectInputStream ois=new ObjectInputStream(new FileInputStream("/data/user/0/nrp107.scarletmail.rutgers.edu.androidphotos/files/data6.dat"));
        ArrayList<Album> albumArrayList= (ArrayList<Album>) ois.readObject();
        ois.close();
        return albumArrayList;
    }
}
