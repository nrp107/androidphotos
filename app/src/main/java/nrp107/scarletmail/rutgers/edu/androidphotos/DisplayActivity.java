package nrp107.scarletmail.rutgers.edu.androidphotos;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import model.Photo;
import model.Tags;

public class DisplayActivity extends AppCompatActivity {

    ListView tagListView;
    EditText tagEditText;
    Button addTagButton;
    Button deleteTagButton;
    Button nextButton;
    Button prevButton;
    ImageView photoImageView;

    //ArrayList<Tags> tagArrayList= new ArrayList<Tags>();
    ArrayList<Tags> tagArrayList= (ArrayList<Tags>) MainActivity.albumArrayList.get(MainActivity.albumIndex).getPhotos().get(AlbumActivity.photoIndex).getTags();
    ArrayAdapter<Tags> adapter;

    int index=AlbumActivity.photoIndex;
    int numPics=MainActivity.albumArrayList.get(MainActivity.albumIndex).getPhotos().size();
    ArrayList<Photo> photos= (ArrayList<Photo>) MainActivity.albumArrayList.get(MainActivity.albumIndex).getPhotos();
    Photo photo=photos.get(AlbumActivity.photoIndex);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        tagListView=(ListView) findViewById(R.id.tagListView);
        tagEditText=(EditText) findViewById(R.id.tagEditText);
        addTagButton=(Button) findViewById(R.id.addTagButton);
        deleteTagButton=(Button) findViewById(R.id.deleteTagButton);
        nextButton=(Button)findViewById(R.id.nextButton);
        prevButton =(Button)findViewById(R.id.prevButton);
        photoImageView=(ImageView) findViewById(R.id.photoImageView);

        //set originally selected photo
        photoImageView.setImageBitmap(photo.selectedImage.getBitmap());


        adapter=new ArrayAdapter<Tags>(this, android.R.layout.simple_list_item_single_choice,tagArrayList );
        tagListView.setAdapter(adapter);

        tagListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //tagEditText.setText(tagArrayList.get(position).getAlbumName());
            }
        });
        addTagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add();
                try {
                    MainActivity.save(MainActivity.albumArrayList);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        deleteTagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
                try {
                    MainActivity.save(MainActivity.albumArrayList);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });

        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prev();
            }
        });
    }

    public void add()
    {
        if (tagEditText.getText().toString().length()>0)
        {
            Tags tag=new Tags(tagEditText.getText().toString().toLowerCase());
            //check for duplicate
            boolean duplicate=false;
            for (int i=0;i<tagArrayList.size();i++)
            {
                if (tagEditText.getText().toString().toLowerCase().equals(tagArrayList.get(i).getTags())){
                    duplicate = true;
                    Toast.makeText(getApplicationContext(),"Tag already exists", Toast.LENGTH_SHORT).show();
                    break;
                }
            }
            if (duplicate==false) {
                adapter.add(tag);
                adapter.notifyDataSetChanged();
                tagEditText.setText("");
                Toast.makeText(getApplicationContext(), "Successfully added tag", Toast.LENGTH_SHORT).show();
            }
        }
        else
            Toast.makeText(getApplicationContext(),"Must enter name to add new tag", Toast.LENGTH_SHORT).show();

    }

    public void delete()
    {
        int position = tagListView.getCheckedItemPosition();
        if (position>-1)
        {
            adapter.remove(tagArrayList.get(position));
            adapter.notifyDataSetChanged();
            tagEditText.setText("");
            Toast.makeText(getApplicationContext(),"Successfully deleted tag ", Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(getApplicationContext(),"Must select album to tag", Toast.LENGTH_SHORT).show();

    }
    public void next()
    {
        if (index<numPics-1)
        {
            index++;
            photo=photos.get(index);
            //show next pic
            photoImageView.setImageBitmap(photo.selectedImage.getBitmap());

            //show next tags
            tagArrayList= (ArrayList<Tags>) photo.getTags();
            adapter=new ArrayAdapter<Tags>(this, android.R.layout.simple_list_item_single_choice,tagArrayList );
            tagListView.setAdapter(adapter);
        }
    }
    public void prev()
    {
        if (index>0)
        {
            index--;
            photo = photos.get(index);
            //show prev pic
            photoImageView.setImageBitmap(photo.selectedImage.getBitmap());
            //show prev tags
            tagArrayList= (ArrayList<Tags>) photo.getTags();
            adapter=new ArrayAdapter<Tags>(this, android.R.layout.simple_list_item_single_choice,tagArrayList );
            tagListView.setAdapter(adapter);
        }
    }

}
