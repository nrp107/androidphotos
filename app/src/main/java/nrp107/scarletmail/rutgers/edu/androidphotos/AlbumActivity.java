package nrp107.scarletmail.rutgers.edu.androidphotos;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import model.Photo;

public class AlbumActivity extends AppCompatActivity {

    ListView photoListView;
    //GridView photoGridView;
    Button addPhotoButton;
    Button removePhotoButton;
    Button movePhotoButton;
    Button displayPhotoButton;
    ImageView picImageView;
    private static int RESULT_LOAD_IMG = 1;


    private static final int PICK_IMAGE=100;
    Uri imageURI;

    ArrayList<Photo> photoArrayList= (ArrayList<Photo>) MainActivity.albumArrayList.get(MainActivity.albumIndex).getPhotos();
    ArrayAdapter<Photo> adapter;

    static int photoIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        photoListView=(ListView) findViewById(R.id.photoListView);
        //photoGridView= (GridView) findViewById(R.id.photoGridView);
        addPhotoButton=(Button) findViewById(R.id.addPhotoButton);
        removePhotoButton=(Button) findViewById(R.id.removePhotoButton);
        movePhotoButton=(Button) findViewById(R.id.movePhotoButton);
        displayPhotoButton=(Button) findViewById(R.id.displayPhotoButton);
        picImageView= (ImageView) findViewById(R.id.picImageView);

        adapter=new ArrayAdapter<Photo>(this, android.R.layout.simple_gallery_item,photoArrayList );
        photoListView.setAdapter(adapter);

        if (photoArrayList.size()>0)
        {
            //photoListView.getSelectedView().setSelected(true);
           // photoListView.getSelectedView().setSelected(true);


        }


        photoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //set thumbnail image
                photoIndex = photoListView.getCheckedItemPosition();
                Photo photo=photoArrayList.get(AlbumActivity.photoIndex);

               // picImageView.setImageURI(photo.imageUri);
                //picImageView.setImageBitmap(BitmapFactory.decodeFile(photo.filePath));
                picImageView.setImageBitmap(photo.selectedImage.getBitmap());


            }
        });

        addPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        removePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove();
            }
        });

        movePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (photoListView.getCheckedItemCount()==1)
                {
                    photoIndex = photoListView.getCheckedItemPosition();
                    Intent moveIntent = new Intent(getApplicationContext(),MoveActivity.class);
                    startActivity(moveIntent);
                }
                else
                    Toast.makeText(getApplicationContext(),"Must select a photo to move", Toast.LENGTH_SHORT).show();
            }

        });

        displayPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (photoListView.getCheckedItemCount()==1)
                {
                    photoIndex = photoListView.getCheckedItemPosition();
                    Intent displayIntent = new Intent(getApplicationContext(),DisplayActivity.class);
                    startActivity(displayIntent);
                }
                else
                    Toast.makeText(getApplicationContext(),"Must select a photo to display", Toast.LENGTH_SHORT).show();
            }
        });



    }

    public void openGallery()
    {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            try {
                 Uri imageUri = data.getData();
                 InputStream imageStream = getContentResolver().openInputStream(imageUri);
                 Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                picImageView.setImageBitmap(selectedImage);
                Photo photo=new Photo(selectedImage);
                adapter.add(photo);
                adapter.notifyDataSetChanged();
                try {
                    MainActivity.save(MainActivity.albumArrayList);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();
               // Toast.makeText(PostImage.this, "Something went wrong", Toast.LENGTH_LONG).show();
            }

        }
    }
    public void remove()
    {
        int position = photoListView.getCheckedItemPosition();
        if (position>-1)
        {
            adapter.remove(photoArrayList.get(position));
            adapter.notifyDataSetChanged();
            try {
                MainActivity.save(MainActivity.albumArrayList);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //autoselect first image here
            if (photoArrayList.size()>0)
            {
                //photoGridView.setSelection(0);
            }
            picImageView.setImageBitmap(null);

            Toast.makeText(getApplicationContext(),"Successfully removed photo "+" arrayList length is ", Toast.LENGTH_SHORT).show();
            //MainActivity.saveSerializable(MainActivity.fixedContext,MainActivity.albumArrayList);

        }
        else
            Toast.makeText(getApplicationContext(),"Must select a photo to remove", Toast.LENGTH_SHORT).show();
    }







}
