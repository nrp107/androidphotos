package nrp107.scarletmail.rutgers.edu.androidphotos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import model.Album;
import model.Photo;

public class MoveActivity extends AppCompatActivity {


    ListView moveAlbumsListView;

    ArrayList<Album> moveAlbumsArrayList=MainActivity.albumArrayList;
    ArrayAdapter<Album> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_move);

        moveAlbumsListView=(ListView) findViewById(R.id.moveAlbumsListView);

        adapter=new ArrayAdapter<Album>(this, android.R.layout.simple_list_item_single_choice,moveAlbumsArrayList );
        moveAlbumsListView.setAdapter(adapter);

        moveAlbumsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int index=moveAlbumsListView.getCheckedItemPosition();
                Toast.makeText(getApplicationContext(), "index is "+index, Toast.LENGTH_SHORT).show();

                Photo photo=MainActivity.albumArrayList.get(MainActivity.albumIndex).getPhotos().get(AlbumActivity.photoIndex);
                MainActivity.albumArrayList.get(index).getPhotos().add(photo);
                MainActivity.albumArrayList.get(MainActivity.albumIndex).getPhotos().remove(AlbumActivity.photoIndex);
                try {
                    MainActivity.save(MainActivity.albumArrayList);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getApplicationContext(), "Successfully moved photo", Toast.LENGTH_SHORT).show();
                Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(mainIntent);
                Intent albumIntent = new Intent(getApplicationContext(), AlbumActivity.class);
                startActivity(albumIntent);
            }
        });
    }
}
