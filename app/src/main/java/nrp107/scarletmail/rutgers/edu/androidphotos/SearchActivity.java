package nrp107.scarletmail.rutgers.edu.androidphotos;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

import model.Album;
import model.Photo;

public class SearchActivity extends AppCompatActivity {

    ListView searchListView;
    EditText searchEditText;
    ImageView picImageView;

    ArrayList<Photo> allPhotosArrayList=new ArrayList<Photo>();
    ArrayAdapter<Photo> adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

//        for (int i=0;i<MainActivity.albumArrayList.size();i++)
//        {
//            for (int k=0;k<MainActivity.albumArrayList.get(i).getPhotos().size();k++)
//            {
//                allPhotosArrayList.add(MainActivity.albumArrayList.get(i).getPhotos().get(k));
//            }
//        }

        searchListView=(ListView) findViewById(R.id.searchListView);
        searchEditText=(EditText) findViewById(R.id.searchEditText);
        picImageView=(ImageView) findViewById(R.id.picImageView);

        adapter=new ArrayAdapter<Photo>(this, android.R.layout.simple_gallery_item,allPhotosArrayList );
        searchListView.setAdapter(adapter);

        initList();
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
//
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals(""))
                {
                    //reset listview
                    initList();
                    adapter.notifyDataSetChanged();
                }
                else
                {
                    //perform search
                    initList();
                    searchItems(s.toString());
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        searchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //set thumbnail image
                int photoIndex = searchListView.getCheckedItemPosition();
                Photo photo=allPhotosArrayList.get(photoIndex);
                picImageView.setImageBitmap(photo.selectedImage.getBitmap());
            }
        });





    }

    public void initList()
    {
        allPhotosArrayList.clear();
        for (int i=0;i<MainActivity.albumArrayList.size();i++)
        {
            Album album=MainActivity.albumArrayList.get(i);
            for (int k=0;k<album.getPhotos().size();k++)
            {
                Photo photo=album.getPhotos().get(k);
                allPhotosArrayList.add(photo);
            }
        }
    }

    public void searchItems(String textToSearch)
    {
        //check info is correct
//        for (int i=0;i<allPhotosArrayList.size();i++)
//        {
//            System.out.println("PHOTO "+i+" size="+allPhotosArrayList.get(i).tags.size());
//            if (allPhotosArrayList.get(i).tags.size()>0)
//            {
//                System.out.print("tags=");
//                for (int k=0;k<allPhotosArrayList.get(i).tags.size();k++)
//                {
//                    System.out.print(allPhotosArrayList.get(i).tags.get(k)+", ");
//                }
//                System.out.print("\n");
//            }
//        }

        boolean match;
        for (int i=0;i<allPhotosArrayList.size();i++)
        {
            if (allPhotosArrayList.get(i).tags.size()==0)
            {
                allPhotosArrayList.remove(allPhotosArrayList.get(i));
                i--;
            }
            else
            {
                match=false;
                for (int k=0;k<allPhotosArrayList.get(i).tags.size();k++)
                {
                    String item=allPhotosArrayList.get(i).tags.get(k).tag;
                    if (item.contains(textToSearch))
                        match=true;
                }
                if (match==false)
                {
                    allPhotosArrayList.remove(allPhotosArrayList.get(i));
                    i--;
                }
            }

        }
        //adapter.notifyDataSetChanged();
    }
}
